# REST API
## Deploy

activate profile-windows    
`$ set spring_profiles_ative=<profile>`

local run
`$ mvnw.cmd spring-boot:run -Dspring-boot.run.profiles=<profile>`

run final solution
``
## Build

clean target
`$ mvnw.cmd clean`

build
`$ mvnw.cmd package`

Docker build
```sh
$ git clone git@gitlab.com:form-cfreire/spring-rest-api.git
$ cd spring-rest-api
$ docker build -t spring-rest-api:latest .
``` 


##CI/CD
` $ sudo apk add gitlab-runner`

`sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "TGcZetS_Pyk5SCs4mysU" \
  --executor "shell" \
  --description "docker-lab-node1" \
  --tag-list "production_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"`

  `$ gitlab-runner run &`



